﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
    class Program
    {

        static void Main(string[] args)
        {
            Polynomial firstPolynomial = new Polynomial(2, new double[] { 1, 1, 2 });
            Polynomial secondPolynomial = new Polynomial(3, new double[] { 1, 1, 1, 1 });

            Console.WriteLine("First polynomial:");
            Console.WriteLine(firstPolynomial);

            Console.WriteLine("Second polynomial:");
            Console.WriteLine(secondPolynomial);

            Console.WriteLine("Result of calculation first polynomial with argument which is 2:");
            Console.WriteLine(firstPolynomial.Calculate(2));

            Console.WriteLine("Second polynomial + first polynomial:");
            Console.WriteLine(secondPolynomial + firstPolynomial);

            Console.WriteLine("First polynomial - second polynomial:");
            Console.WriteLine(firstPolynomial - secondPolynomial);

            Console.WriteLine("First polynomial * second polynomial:");
            Console.WriteLine(firstPolynomial * secondPolynomial);

            Console.ReadKey();
        }
    }
}
