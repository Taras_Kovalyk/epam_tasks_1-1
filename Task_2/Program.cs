﻿using System;
namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle firstRectangle = new Rectangle(new Point(-5, -5), 10, 7);
            Rectangle secondRectangle = new Rectangle(new Point(0, 0), 3, 4);

            Console.WriteLine("First rectangle:");
            Console.WriteLine(firstRectangle);

            Console.WriteLine("Second rectangle:");
            Console.WriteLine(secondRectangle);

            Console.WriteLine("Rectangle that is an intersection of first and second rectangles:");
            try
            {
                Console.WriteLine(Rectangle.Intersect(firstRectangle, secondRectangle));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Rectangle that is the smallest union of first and second rectangles:");
            Console.WriteLine(Rectangle.MinUnion(firstRectangle, secondRectangle));

            firstRectangle.Resize(7, 12);
            Console.WriteLine("First rectangle after resizing (width = 7; heigth = 12)");
            Console.WriteLine(firstRectangle);

            secondRectangle.Move(2, 5);
            Console.WriteLine("Second rectangle after moving (delta X = 2; delta Y = 5)");
            Console.WriteLine(secondRectangle);
            Console.ReadKey();
        }
    }
}
