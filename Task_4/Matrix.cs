﻿using System;
using System.Text;

namespace Task_4
{
    /// <summary>
    /// Type that is a rectangular array of quantities or expressions in rows and columns that is treated as a single entity and manipulated according to particular rules
    /// </summary>
    public class Matrix
    {
        private double[,] _matrix;
        public int ColumnCount { get; private set; }
        public int RowCount { get; private set; }

        #region Constructors
        public Matrix(int rowCount, int columnCount)
        {
            _matrix = new double[rowCount, columnCount];
            RowCount = rowCount;
            ColumnCount = columnCount;
        }

        public Matrix(double[,] matrix)
        {
            _matrix = matrix;
            RowCount = matrix.GetLength(0);
            ColumnCount = matrix.GetLength(1);
        }
        #endregion
        private static Matrix PerformOperation(Matrix a, Matrix b, Func<double, double, double> operation)
        {
            if (a.ColumnCount != b.ColumnCount || a.RowCount != b.RowCount)
            {
                throw new InvalidOperationException("Operation could not be performed to matrices with different dimensions");
            }

            var resultMatrix = new Matrix(a.RowCount, b.ColumnCount);
            for (var i = 0; i < a.RowCount; i++)
            {
                for (var j = 0; j < a.ColumnCount; j++)
                {
                    resultMatrix[i, j] = operation(a[i, j], b[i, j]);
                }
            }
            return resultMatrix;
        }

        private static double DeterminantOfTriangularMatrix(Matrix matrix)
        {
            double determinant = 1;
            for (var i = 0; i < matrix.ColumnCount; i++)
            {
                determinant *= matrix[i, i];
            }
            return determinant;
        }

        public static Matrix operator +(Matrix left, Matrix right)
        {
            return PerformOperation(left, right, (x, y) => x + y);
        }

        public static Matrix operator -(Matrix left, Matrix right)
        {
            return PerformOperation(left, right, (x, y) => x - y);
        }

        public static Matrix operator *(Matrix left, Matrix right)
        {
            if (left.ColumnCount != right.RowCount && left.RowCount != right.ColumnCount)
            {
                throw new InvalidOperationException("Multiplication could not be performed to matrices with different dimensions");
            }

            var resultMatrix = new Matrix(left.RowCount, right.ColumnCount);
            for (var i = 0; i < left.RowCount; i++)
            {
                for (var j = 0; j < right.ColumnCount; j++)
                {
                    for (var k = 0; k < left.ColumnCount; k++)
                    {
                        resultMatrix[i, j] += left[i, k] * right[k, j];
                    }
                }
            }
            return resultMatrix;
        }

        public double this[int i, int j]
        {
            get { return _matrix[i, j]; }

            set { _matrix[i, j] = value; }
        }
      
        /// <summary>
        /// Calculation of matrix determinant
        /// </summary>
        /// <returns></returns>
        public double GetDeterminant()
        {
            if (ColumnCount != RowCount)
            {
                throw new InvalidOperationException("Determinant could not be calculated for not square matrix");
            }

            var leftTriangularMatrix = new Matrix(ColumnCount, ColumnCount);
            var rightTriangularMatrix = new Matrix(ColumnCount, ColumnCount);
            for (var i = 0; i < ColumnCount; i++)
            {
                rightTriangularMatrix[i, i] = 1;
            }

            for (var k = 0; k < ColumnCount; k++)
            {
                for (var i = 0; i < ColumnCount; i++)
                {
                    leftTriangularMatrix[i, k] = _matrix[i, k];
                    for (var p = 0; p < k; p++)
                    {
                        leftTriangularMatrix[i, k] -= leftTriangularMatrix[i, p] * rightTriangularMatrix[p, k];
                    }
                }

                for (var j = k + 1; j < ColumnCount; j++)
                {
                    rightTriangularMatrix[k, j] = _matrix[k, j];
                    for (var p = 0; p < k; p++)
                    {
                        rightTriangularMatrix[k, j] -= leftTriangularMatrix[k, p] * rightTriangularMatrix[p, j];
                    }

                    rightTriangularMatrix[k, j] /= leftTriangularMatrix[k, k];
                }
            }
            return DeterminantOfTriangularMatrix(leftTriangularMatrix);
        }

        /// <summary>
        /// Calculation minor of order
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public double FindFirstMinorOfOrder(int order)
        {
            if (order <= 0 || order > RowCount || order > ColumnCount)
            {
                throw new ArgumentException("Minor could not be found for this order");
            }

            var minorMatrix = new Matrix(order, order);
            order -= 1;
            for (var i = 0; i <= order; i++)
            {
                for (var j = 0; j <= order; j++)
                {
                    minorMatrix[i, j] = _matrix[i, j];
                }
            }

            return minorMatrix.GetDeterminant();
        }

        /// <summary>
        /// Calculation complement minor of order
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public double FindComplementMinorOfOrder(int order)
        {
            if (order <= 0 || order >= RowCount || order >= ColumnCount)
            {
                throw new ArgumentException("Minor could not be found for this order");
            }

            if (ColumnCount != RowCount)
            {
                throw new InvalidOperationException("Determinant could not be calculated for not square matrix");
            }

            var minorMatrix = new Matrix(ColumnCount - order, ColumnCount - order);
            order -= 1;
            for (var i = order + 1; i < ColumnCount; i++)
            {
                for (var j = order + 1; j < ColumnCount; j++)
                {
                    minorMatrix[i - order - 1, j - order - 1] = _matrix[i, j];
                }
            }
            return minorMatrix.GetDeterminant();
        }

        /// <summary>
        /// Calculation minor of element by rowIndex and columnIndex
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <returns></returns>
        public double FindMinorij(int rowIndex, int columnIndex)
        {
            rowIndex -= 1;
            columnIndex -= 1;
            if (rowIndex < 0 || rowIndex >= RowCount || columnIndex < 0 || columnIndex >= ColumnCount)
            {
                throw new IndexOutOfRangeException("Indeces are out of range");
            }

            if (ColumnCount != RowCount)
            {
                throw new InvalidOperationException("Determinant could not be calculated for not square matrix");
            }

            var matrixWithoutRowAndColumn = new Matrix(RowCount - 1, ColumnCount - 1);
            for (var i = 0; i < rowIndex; i++)
            {
                for (var j = 0; j < columnIndex; j++)
                {
                    matrixWithoutRowAndColumn[i, j] = _matrix[i, j];
                }
            }

            for (var i = 0; i < rowIndex; i++)
            {
                for (var j = columnIndex + 1; j < ColumnCount; j++)
                {
                    matrixWithoutRowAndColumn[i, j - 1] = _matrix[i, j];
                }
            }

            for (var i = rowIndex + 1; i < RowCount; i++)
            {
                for (var j = 0; j < columnIndex; j++)
                {
                    matrixWithoutRowAndColumn[i - 1, j] = _matrix[i, j];
                }
            }

            for (var i = rowIndex + 1; i < RowCount; i++)
            {
                for (var j = columnIndex + 1; j < ColumnCount; j++)
                {
                    matrixWithoutRowAndColumn[i - 1, j - 1] = _matrix[i, j];
                }
            }
            return matrixWithoutRowAndColumn.GetDeterminant();
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            for (var i = 0; i < RowCount; i++)
            {
                for (var j = 0; j < ColumnCount; j++)
                {
                    result.Append(_matrix[i, j] + " ");
                }
                result.Append(Environment.NewLine);
            }
            return result.ToString();
        }
    }
}
