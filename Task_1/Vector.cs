﻿using System;
using System.Text;

namespace Task_1
{
    /// <summary>
    /// Vector - one-dimensional array of integers
    /// </summary>
    public class Vector
    {
        private int[] Array { get; set; }
        public int Length { get; private set; }
        public int IndexFrom { get; private set; }

        #region Constructors
        public Vector(int length)
        {
            Array = new int[length];
            Length = Array.Length;
            IndexFrom = 0;
        }

        public Vector(int indexFrom, int indexTo)
        {
            if (indexTo <= indexFrom)
                throw new ArgumentException();
            IndexFrom = indexFrom;
            if (indexFrom < 0 && indexTo >= 0)
                Length = Math.Abs(indexFrom) + Math.Abs(indexTo);
            else
                Length = Math.Abs(indexTo) - Math.Abs(indexFrom);
            Array = new int[Length];
        }
        #endregion

        #region Operations
        public static Vector operator +(Vector left, Vector right)
        {
            if ((left.Length == right.Length) && (left.IndexFrom == right.IndexFrom))
            {
                Vector result = new Vector(left.IndexFrom, left.IndexFrom + left.Length);
                for (int i = left.IndexFrom; i < left.Length+left.IndexFrom; i++)
                    result[i] = left[i] + right[i];

                return result;
            }
            else
                throw new ArgumentException("Parameteres of vectors are not equal");
        }

        public static Vector operator -(Vector left, Vector right)
        {
            if (left.Length == right.Length && (left.IndexFrom == right.IndexFrom))
            {
                Vector result = new Vector(left.IndexFrom, left.IndexFrom + left.Length);
                for (int i = left.IndexFrom; i < left.Length+ left.IndexFrom; i++)
                    result[i] = left[i] - right[i];
                return result;
            }
            else
                throw new ArgumentException("Parameteres of vectors are not equal");
        }

        public int this[int i]
        {
            get
            {
                if ((i - IndexFrom) < Array.Length && (i - IndexFrom) >= 0)
                    return Array[i - IndexFrom];
                else
                    throw new IndexOutOfRangeException();
            }
            set
            {
                if ((i - IndexFrom) < Array.Length && (i - IndexFrom) >= 0)
                    Array[i - IndexFrom] = value;
                else
                    throw new IndexOutOfRangeException();
            }
        }

        /// <summary>
        /// Multiply vector elements by value.
        /// </summary>
        /// <param name="value"></param>
        public void MultiplyByValue(int value)
        {
            for (int i = 0; i < Array.Length; i++)
                Array[i] *= value;
        }

        public override bool Equals(object obj)
        {
            Vector vector = obj as Vector;
            if (vector == null)
                return false;
            if (!Array.Equals(vector.Array))
                for (int i = 0; i < vector.Length; i++)
                    if (!(Array[i] == vector.Array[i]))
                        return false;
            return true;
        }

        public override int GetHashCode()
        {
            return Array.GetHashCode();
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder(Array.Length * 2);
            for (int i = 0; i < Array.Length; i++)
                result.Append(Array[i] + " ");
            return result.ToString();
        }

        #endregion
    }
}
