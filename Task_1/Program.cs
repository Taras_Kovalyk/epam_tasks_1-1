﻿using System;

namespace Task_1
{
    class Program
    {
        static void DisplayVector(Vector vector)
        {
            for (int i = vector.IndexFrom; i < vector.Length + vector.IndexFrom; i++)
                Console.Write(vector[i] + " ");
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            Vector firstVector = new Vector(-2, 4);
            Vector secondVector = new Vector(-2, 4);
            for (int i = firstVector.IndexFrom; i < firstVector.Length + firstVector.IndexFrom; i++)
            {
                firstVector[i] = i;
                secondVector[i] = i + 3;
            }

            Console.WriteLine("First Vector:");
            DisplayVector(firstVector);

            Console.WriteLine("Second Vector:");
            DisplayVector(secondVector);

            firstVector.MultiplyByValue(2);
            Console.WriteLine("Fist Vector after multiplying by 2");
            DisplayVector(firstVector);

            Console.WriteLine("New Vector after adding first and second Vectors:");
            try
            {
                DisplayVector(firstVector + secondVector);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("New Vector after subtraction first and second Vectors:");
            try
            {
                DisplayVector(firstVector - secondVector);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Is first Vector equals to the second one?");
            Console.WriteLine(firstVector.Equals(secondVector));

            Console.ReadKey();
        }
    }
}
